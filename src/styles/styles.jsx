import { mode } from '@chakra-ui/theme-tools';

export const styles = {
  global: (props) => ({
    'html, body': {
      fontSize: 'sm',
      fontWeight: 'medium',
      _dark: {
        color: 'gray.50',
        bg: 'gray.900',
      },
      color: mode('whiteAlpha.900', 'whiteAlpha.900')(props),
      lineHeight: 'tall',
      backgroundColor: mode('#0D4545', '#0D4545')(props),
    },
  }),
};
