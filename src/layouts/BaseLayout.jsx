import { Box } from '@chakra-ui/react';

export function BaseLayout({ children }) {
  return <Box>{children}</Box>;
}
