import { Box, Stack } from '@chakra-ui/react';
import Sidebar from '../components/sideBar';

export function AuthLayout({ children }) {
  return (
    <Stack direction="row">
      <Sidebar />
      <Box align="stretch" h="100%" w="100%" p={4}>
        {children}
      </Box>
    </Stack>
  );
}
