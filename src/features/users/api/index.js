import axios, { AxiosError } from 'axios';

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});
/**
 * @param {{email: string, senha: string}} credentials
 */
export async function signIn(credentials) {
  try {
    const res = await api.post('/users/sign-in', credentials);
    return res.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new Error(error.response.data.message);
    }
    throw new Error(error.message);
  }
}

/**
 * @param {{nome: string, email: string, senha: string}} userPayload
 */
export async function signUp(userPayload) {
  try {
    const res = await api.post('/users/sign-up', userPayload);
    return res.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new Error(error.response.data.message);
    }
    throw new Error(error.message);
  }
}

/**
 *
 * @param {string} email
 */
export async function getByEmail(email) {
  try {
    const res = await api.get('/users', { params: { email } });

    return res.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new Error(error.response.data.message);
    }
    throw new Error(error.message);
  }
}

export async function getById(id) {
  try {
    const res = await api.get(`/users/${id}`);
    return res.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new Error(error.response.data.message);
    }
    throw new Error(error.message);
  }
}

/**
 * @param {{nome: string, email: string, senha: string}} userUpdatePayload
 */
export async function putUserData(userUpdatePayload) {
  try {
    const res = await api.put(
      `/users/sign-in/${userUpdatePayload.email}`,
      userUpdatePayload
    );
    return res.data;
  } catch (error) {
    if (error instanceof AxiosError) {
      throw new Error(error.response.data.message);
    }
    throw new Error(error.message);
  }
}
