import { Stack } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';

/**
 * @typedef {Object} userData
 * @property {string} name - O nome do usuário
 * @property {string} email - O email do usuário
 */

/**
 * @typedef {Object} submitData
 * @property {string} name - O nome do usuário
 * @property {string} email - O email do usuário
 * @property {string} newPassowrd - A nova senha do usuário
 */

/**
 *
 * @param {{userData?: userData, submit: (userData: newPassowrd) => void}} props
 */
export function UserProfileForm({ userData, submit }) {
  const { watch, handleSubmit } = useForm({
    defaultValues: { ...userData, password: '', confirmPassword: '' },
  });
  return <Stack />;
}
