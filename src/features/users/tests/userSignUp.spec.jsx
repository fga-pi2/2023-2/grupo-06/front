import { faker } from '@faker-js/faker';

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { vi } from 'vitest';
import { UserForm } from '../CreateOrUpdateUser';
import { Wrapper } from '../../../utils/testWrapper';

const cadastroMock = {
  nome: faker.person.fullName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
};

const mockSubmit = vi.fn(async (data) => {
  Promise.resolve(data);
});
describe('UserForm', () => {
  describe('Ao se cadastrar', () => {
    const expectedErrors = [
      ['nome', 'Necessário inserir o nome'],
      ['email', 'Necessário inserir o email'],
      ['senha', 'Necessário inserir a senha'],
      ['confirmar senha', 'Necessário inserir a confirmação de senha'],
    ];
    it.each(expectedErrors)(
      'deve mostrar erro de %s vázio ao tentar se cadastrar',
      async (name, input) => {
        render(
          <Wrapper>
            <UserForm submit={mockSubmit} />
          </Wrapper>
        );
        const submitButton = screen.getByText('Criar Conta');
        await waitFor(() => {
          fireEvent.submit(submitButton);
          const text = screen.getByText(input);
          expect(text).toBeInTheDocument();
        });
      }
    );
    it('deve mostrar um erro caso as senhas sejam diferentes', async () => {
      render(
        <Wrapper>
          <UserForm submit={mockSubmit} />
        </Wrapper>
      );
      const submitButton = screen.getByText('Criar Conta');
      await waitFor(() => {
        fireEvent.input(screen.getByLabelText('Nome'), {
          target: {
            value: cadastroMock.nome,
          },
        });
        fireEvent.input(screen.getByLabelText('E-mail'), {
          target: {
            value: cadastroMock.email,
          },
        });

        fireEvent.input(screen.getByLabelText('Senha'), {
          target: {
            value: cadastroMock.password,
          },
        });
        fireEvent.input(screen.getByLabelText('Confirmar Senha'), {
          target: {
            value: 'some password',
          },
        });

        fireEvent.submit(submitButton);
        const text = screen.getAllByText('As senhas devem ser iguais');
        expect(text[0]).toBeInTheDocument();
      });
    });

    it('deve cadastrar o usuário ao preencher corretamente o formulário', async () => {
      render(
        <Wrapper>
          <UserForm submit={mockSubmit} />
        </Wrapper>
      );
      const submitButton = screen.getByText('Criar Conta');
      await waitFor(() => {
        fireEvent.input(screen.getByLabelText('Nome'), {
          target: {
            value: cadastroMock.nome,
          },
        });
        fireEvent.input(screen.getByLabelText('E-mail'), {
          target: {
            value: cadastroMock.email,
          },
        });

        fireEvent.input(screen.getByLabelText('Senha'), {
          target: {
            value: cadastroMock.password,
          },
        });
        fireEvent.input(screen.getByLabelText('Confirmar Senha'), {
          target: {
            value: cadastroMock.password,
          },
        });

        fireEvent.submit(submitButton);

        expect(mockSubmit).toHaveBeenCalled();
      });
    });
  });
  describe('Ao Editar o Perfil', () => {
    it('deve mostrar as informações do usuário nos inputs', () => {
      const userData = {
        name: faker.person.fullName(),
        email: faker.internet.email(),
      };
      render(
        <Wrapper>
          <UserForm userData={userData} submit={mockSubmit} />
        </Wrapper>
      );

      expect(screen.getByDisplayValue(userData.name)).toBeInTheDocument();
      expect(screen.getByDisplayValue(userData.email)).toBeInTheDocument();
    });
  });
  it('deve atualizar o nome de usuário, quando o usuário confirmar as alterações', async () => {
    let submitedName = 'some name';
    const updateUser = vi.fn(async (data) => {
      submitedName = data.name;
    });
    const userData = {
      name: faker.person.fullName(),
      email: faker.internet.email(),
    };
    render(
      <Wrapper>
        <UserForm userData={userData} submit={updateUser} />
      </Wrapper>
    );

    const newName = 'some name';
    // const newName = faker.person.fullName();
    await waitFor(() => {
      const submitButton = screen.getByText('Editar Conta');
      fireEvent.input(screen.getByLabelText('Nome'), {
        target: {
          value: newName,
        },
      });

      fireEvent.submit(submitButton);
      expect(submitedName).toBe(newName);
    });
  });
  /*
  it('deve atualizar a senha do usuário, quando o usuário inserir a senha e a confirmação de nova senha', async () => {
    let submitedPassword = '';
    const updateUser = vi.fn(async (data) => {
      submitedPassword = data.senha;
    });
    const userData = {
      name: faker.person.fullName(),
      email: faker.internet.email(),
    };
    render(
      <Wrapper>
        <UserForm userData={userData} submit={updateUser} />
      </Wrapper>
    );

    await waitFor(() => {
      const submitButton = screen.getByText('Editar Conta');
      fireEvent.input(screen.getByLabelText('Nova Senha'), {
        target: {
          value: cadastroMock.password,
        },
      });
      fireEvent.input(screen.getByLabelText('Confirmar a nova senha'), {
        target: {
          value: cadastroMock.password,
        },
      });

      fireEvent.submit(submitButton);

      expect(submitedPassword).toBe(cadastroMock.password);
    });
  }); */
});
