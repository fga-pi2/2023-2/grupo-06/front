import {
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Image,
  Input,
  VStack,
  Text,
  Switch,
  Spacer,
  Flex,
} from '@chakra-ui/react';

import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

/**
 * @typedef {Object} userData
 * @property {string} name - O nome do usuário
 * @property {string} email - O email do usuário
 */

/**
 * @typedef {Object} submitData
 * @property {string} name - O nome do usuário
 * @property {string} email - O email do usuário
 * @property {string} newPassowrd - A nova senha do usuário
 */

/**
 *
 * @param {{userData?: userData, submit: (userData: newPassowrd) => void}} props
 */
export function UserForm({ userData, submit }) {
  const [showPasswordFields, setShowPasswordFields] = useState(false);
  const togglePasswordFields = () => {
    setShowPasswordFields(!showPasswordFields);
  };
  const isUpdating = !!userData;
  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = async ({ name, email, password, confirmPassword }) => {
    if (isUpdating && (!password || !confirmPassword)) {
      submit({ nome: name, email, senha: password });
      return;
    }
    if (!isUpdating && (!password || !confirmPassword)) {
      submit({ nome: name, email, senha: password });
      return;
    }
    if (password !== confirmPassword) {
      setError('password', {
        type: 'custom',
        message: 'As senhas devem ser iguais',
      });
      setError('confirmPassword', {
        type: 'custom',
        message: 'As senhas devem ser iguais',
      });
      return;
    }

    submit({ nome: name, email, senha: password });
  };

  useEffect(() => {
    if (userData) {
      setValue('name', userData.name);
      setValue('email', userData.email);

      // Define os valores dos campos de senha com base no estado do switch
      if (showPasswordFields) {
        setValue('password', userData.senha || ''); // Define como nulo se não houver senha
        setValue('confirmPassword', userData.senha || ''); // Define como nulo se não houver senha
      } else {
        setValue('password', null);
        setValue('confirmPassword', null);
      }
    }
  }, [setValue, userData, showPasswordFields]);

  return (
    <Center aria-label="form" backgroundColor="#0D4545" color="white">
      <VStack
        align="stretch"
        color="white"
        maxW="sm"
        pb={10}
        py={isUpdating ? undefined : 10}
      >
        <VStack spacing={6} mb={6}>
          <Image
            boxSize="30%"
            fit="cover"
            src="/simple-logo.png"
            alt="logo simplificada"
          />
          <Text fontSize="4xl">{isUpdating ? 'Perfil' : 'Cadastro'}</Text>
        </VStack>
        <form onSubmit={handleSubmit(onSubmit)}>
          <VStack align="stretch" justifyContent="center">
            <FormControl isInvalid={errors.name}>
              <FormLabel htmlFor="name">Nome</FormLabel>
              <Input
                id="name"
                {...register('name', { required: 'Necessário inserir o nome' })}
                size="md"
                fontSize="md"
                variant="primary"
                placeholder="Digite o nome"
              />
              <FormErrorMessage>{errors.name?.message}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.email}>
              <FormLabel htmlFor="email">E-mail</FormLabel>
              <Input
                id="email"
                type="email"
                {...register('email', {
                  required: 'Necessário inserir o email',
                })}
                size="md"
                fontSize="md"
                variant="primary"
                placeholder="Digite o email"
                isDisabled={isUpdating}
              />
              <FormErrorMessage>{errors.email?.message}</FormErrorMessage>
            </FormControl>
            {isUpdating && (
              <>
                <Flex>
                  <Text color="white" fontSize="md" mb="1vh">
                    Trocar de senha?
                  </Text>
                  <Spacer />
                  <Switch
                    alignContent="end"
                    colorScheme="green"
                    mb="1.5vh"
                    onChange={togglePasswordFields}
                    isChecked={showPasswordFields}
                  />
                </Flex>
                {showPasswordFields && ( // Renderiza CadastroPlantacaoModal se o Switch estiver desativado
                  <>
                    <FormControl isInvalid={errors.password}>
                      <FormLabel htmlFor="password">Nova Senha</FormLabel>
                      <Input
                        id="password"
                        size="md"
                        type="password"
                        {...register('password', {
                          required: 'Necessário inserir a senha',
                        })}
                        fontSize="md"
                        variant="primary"
                        placeholder="Digite a senha"
                      />
                      <FormErrorMessage>
                        {errors.password?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={errors.confirmPassword}>
                      <FormLabel htmlFor="confirmPassword">
                        Confirmar Nova Senha
                      </FormLabel>
                      <Input
                        id="confirmPassword"
                        size="md"
                        type="password"
                        {...register('confirmPassword', {
                          required: 'Necessário inserir a confirmação de senha',
                        })}
                        fontSize="md"
                        variant="primary"
                        placeholder="Digite novamente a senha"
                      />
                      <FormErrorMessage>
                        {errors.confirmPassword?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </>
                )}
              </>
            )}
            {!isUpdating && (
              <>
                <FormControl isInvalid={errors.password}>
                  <FormLabel htmlFor="password">Senha</FormLabel>
                  <Input
                    id="password"
                    size="md"
                    type="password"
                    {...register('password', {
                      required: 'Necessário inserir a senha',
                    })}
                    fontSize="md"
                    variant="primary"
                    placeholder="Digite a senha"
                  />
                  <FormErrorMessage>
                    {errors.password?.message}
                  </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={errors.confirmPassword}>
                  <FormLabel htmlFor="confirmPassword">
                    Confirmar Senha
                  </FormLabel>
                  <Input
                    id="confirmPassword"
                    size="md"
                    type="password"
                    {...register('confirmPassword', {
                      required: 'Necessário inserir a confirmação de senha',
                    })}
                    fontSize="md"
                    variant="primary"
                    placeholder="Digite novamente a senha"
                  />
                  <FormErrorMessage>
                    {errors.confirmPassword?.message}
                  </FormErrorMessage>
                </FormControl>
              </>
            )}
            <Button mt={6} variant="primary" type="submit">
              {isUpdating ? 'Editar Conta' : 'Criar Conta'}
            </Button>
          </VStack>
        </form>
      </VStack>
    </Center>
  );
}
