import { useState } from 'react';
import {
  Button,
  Center,
  Box,
  Input,
  Link,
  Grid,
  Image,
  FormControl,
  FormLabel,
  FormErrorMessage,
  VStack,
  useToast,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { useNavigate, useLocation } from 'react-router-dom';
import { useMutation } from '@tanstack/react-query';
// import useLoginData from '../../../contexts/authContext';
// import { useAuth } from '@/contexts/AuthContext';

/**
 *
 * @param {{submit: (payload: {email: string, senha: string}) =>  Promise<void>}} props
 * @returns
 */
export function SignIn({ submit }) {
  // const { signIn } = useAuth();
  const location = useLocation();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const toast = useToast({ position: 'bottom' });
  const mutation = useMutation({
    mutationFn: submit,
    onSuccess: (data) => {
      localStorage.setItem('userValue', JSON.stringify(data.user));
      toast({
        title: 'Login realizado.',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });

      setTimeout(() => {
        if (location.pathname === '/login') {
          navigate('/plantacao');
        } else {
          navigate('/plantacao');
          window.location.reload();
        }
      }, 1000);
    },
    onError: (err) => {
      toast({
        title: 'Não foi possível logar',
        description: err.message,
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });
  const { isPending: loading } = mutation;
  const onSubmit = async ({ email, password }) => {
    mutation.mutate({ email, senha: password });
  };

  const property = {
    imageUrl: '/logo.png',
    imageAlt: 'Exemplo',
  };

  return (
    <Center aria-label="form" h="100vh" color="white">
      <form onSubmit={handleSubmit(onSubmit)} aria-label="form">
        <VStack align="stretch" color="white" maxW="sm" p={10}>
          <Box marginBottom={10} display="flex" justifyContent="center">
            <Image
              boxSize="50%"
              fit="cover"
              src={property.imageUrl}
              alt={property.imageAlt}
            />
          </Box>

          <Box>
            <FormControl isInvalid={errors.email}>
              <FormLabel>E-mail</FormLabel>
              <Input
                size="md"
                type="email"
                fontSize="md"
                variant="primary"
                {...register('email', { required: true })}
                placeholder="E-mail"
              />
              <FormErrorMessage>E-mail obrigatório.</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.password}>
              <FormLabel>Senha</FormLabel>
              <Input
                size="md"
                fontSize="md"
                variant="primary"
                {...register('password', { required: true })}
                type="password"
                placeholder="Digite sua senha"
              />
              <FormErrorMessage>Senha obrigatória</FormErrorMessage>
            </FormControl>
            <Center>
              <Link
                color="#7CC57A"
                fontSize="sm"
                fontWeight="medium"
                href="/" // É necessário colocar o caminho correto quando for desenvolvido a tela de recuperação de senha
                mt="5px"
              >
                Esqueci a senha
              </Link>
            </Center>
          </Box>

          <VStack align="stretch" spacing={4}>
            <Button variant="primary" type="submit" isLoading={loading}>
              Entrar
            </Button>
            <Button
              w="100%"
              variant="secondary"
              onClick={() => navigate('/cadastroUsuario')} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
            >
              Cadastre-se
            </Button>
          </VStack>
        </VStack>
      </form>
    </Center>
  );
}
