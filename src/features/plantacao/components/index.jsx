import React, { useState, useEffect } from 'react';
import {
  chakra,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Text,
  Stack,
  Box,
  Input,
  Button,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  Tooltip,
  Divider,
  FormControl,
  FormLabel,
  FormErrorMessage,
  ModalFooter,
  VStack,
  useDisclosure,
  Center,
  useToast,
} from '@chakra-ui/react';
import { useForm, Controller } from 'react-hook-form';
import { AddIcon } from '@chakra-ui/icons';
import { usePutUpdateConfiguracao } from '../../configuracao/api/put-update-configuracao';

function SliderThumbWithTooltip({ name, value, setValue }) {
  const [showTooltip, setShowTooltip] = useState(false);

  return (
    <Slider
      id={`slider-${name}`}
      value={value}
      min={0}
      max={100}
      colorScheme="green"
      onChange={(v) => setValue(v)}
      onMouseEnter={() => setShowTooltip(true)}
      onMouseLeave={() => setShowTooltip(false)}
    >
      <SliderMark value={5} mt="1" fontSize="sm">
        0%
      </SliderMark>
      <SliderMark value={93} mt="1" fontSize="sm">
        100%
      </SliderMark>
      <SliderTrack>
        <SliderFilledTrack />
      </SliderTrack>
      <Tooltip
        hasArrow
        bg="teal.500"
        color="white"
        placement="top"
        isOpen={showTooltip}
        label={`${value}%`}
      >
        <SliderThumb />
      </Tooltip>
    </Slider>
  );
}

const PRESET_FORM_ID = 'preset-form';

export function CadastroPlantacaoModal({ isEditing }) {
  const toast = useToast({ position: 'bottom' });
  const [defaultValues, setDefaultValues] = useState({
    id: '',
    name: '',
    autoIrrigate: false,
    temperature: {
      min: '',
      max: '',
    },
    speed: {
      min: '',
      max: '',
    },
    humidadeSoloMin: '',
    humidadeSoloMax: '',
    humidadeArMin: '',
    humidadeArMax: '',
  });

  useEffect(() => {
    setDefaultValues({
      id: isEditing ? isEditing.id : '',
      name: isEditing ? isEditing.nome : '',
      autoIrrigate: false,
      temperature: {
        min: isEditing ? isEditing.temperatura_min : '',
        max: isEditing ? isEditing.temperatura_max : '',
      },
      speed: {
        min: isEditing ? isEditing.velocidade_ar_min : '',
        max: isEditing ? isEditing.velocidade_ar_max : '',
      },
      humidadeSoloMin: isEditing ? isEditing.umidade_solo_min : '',
      humidadeSoloMax: isEditing ? isEditing.umidade_solo_max : '',
      humidadeArMin: isEditing ? isEditing.umidade_ar_min : '',
      humidadeArMax: isEditing ? isEditing.umidade_ar_max : '',
    });
  }, [isEditing]); // Dependência para isEditing

  const {
    control,
    register,
    handleSubmit,
    watch,
    trigger,
    formState: { errors },
  } = useForm({ defaultValues });

  const mutation = usePutUpdateConfiguracao();

  const onUpdate = async () => {
    mutation.mutate({
      id: defaultValues.id,
      umidade_solo_max: defaultValues.humidadeSoloMax,
      umidade_ar_max: defaultValues.humidadeArMax,
      velocidade_ar_max: defaultValues.speed.max,
      temperatura_max: defaultValues.temperature.max,
      umidade_solo_min: defaultValues.humidadeSoloMin,
      umidade_ar_min: defaultValues.humidadeArMin,
      velocidade_ar_min: defaultValues.speed.min,
      temperatura_min: defaultValues.temperature.min,
      nome: defaultValues.name,
    });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  function validateTemperature(value) {
    if (Number(value) < Number(defaultValues.temperature.min)) {
      return true;
    }
    return false;
  }

  function validateSpeed(value) {
    if (Number(value) < Number(defaultValues.speed.min)) {
      return true;
    }
    return false;
  }

  function validateHumidadeSolo(value) {
    if (Number(value) < Number(defaultValues.humidadeSoloMin)) {
      return true;
    }
    return false;
  }

  function validateHumidadeAr(value) {
    if (Number(value) < Number(defaultValues.humidadeArMin)) {
      return true;
    }
    return false;
  }

  function validateAll() {
    if (
      !validateHumidadeSolo(defaultValues.humidadeSoloMax) &&
      !validateHumidadeAr(defaultValues.humidadeArMax) &&
      !validateSpeed(defaultValues.speed.max) &&
      !validateTemperature(defaultValues.temperature.max)
    ) {
      return true;
    }
    return false;
  }

  return (
    <>
      <Center>
        <Tooltip
          label="Definir Nova Configuração"
          aria-label="Definir Nova Configuração"
        >
          <Button
            fontWeight="normalbold"
            variant="primary"
            type="submit"
            maxW="10vw"
            maxH="5vh"
            onClick={onOpen}
          >
            Editar configuração
          </Button>
        </Tooltip>
      </Center>
      <Modal size="xl" isOpen={isOpen} onClose={onClose}>
        <ModalOverlay backdropFilter="blur(3px)" />
        <ModalContent bg="#0A7176">
          <ModalHeader textAlign="center" fontSize="2xl">
            Editar Configuração
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <chakra.form id={PRESET_FORM_ID}>
              <FormControl isInvalid={errors.name}>
                <FormLabel>Nome da configuração</FormLabel>
                <Input
                  variant="primary"
                  value={defaultValues.name}
                  onChange={(e) =>
                    setDefaultValues({ ...defaultValues, name: e.target.value })
                  }
                  placeholder="Digite o nome da configuração"
                />
                <FormErrorMessage>{errors.name?.message}</FormErrorMessage>
              </FormControl>
              <Divider marginTop="15px" />
              <Stack direction={['column', 'row']} spacing={4} mb={2}>
                {/* Conteúdo da modal */}
                <VStack w={['100%', '50%']} alignContent="stretch">
                  <Box w="100%" my={4}>
                    <Text>Umidade mínima do solo</Text>
                    <Controller
                      control={control}
                      name="humidadeSoloMin"
                      render={({ field }) => (
                        <SliderThumbWithTooltip
                          name="humidadeSoloMin"
                          value={defaultValues.humidadeSoloMin}
                          setValue={(value) => {
                            field.onChange(value);
                            setDefaultValues({
                              ...defaultValues,
                              humidadeSoloMin: value,
                            });
                          }}
                        />
                      )}
                    />
                  </Box>
                  <Box w="100%" my={4}>
                    <Text>Umidade mínima do ar</Text>
                    <Controller
                      control={control}
                      name="humidadeArMin"
                      render={({ field }) => (
                        <SliderThumbWithTooltip
                          name="humidadeArMin"
                          value={defaultValues.humidadeArMin}
                          setValue={(value) => {
                            field.onChange(value);
                            setDefaultValues({
                              ...defaultValues,
                              humidadeArMin: value,
                            });
                          }}
                        />
                      )}
                    />
                  </Box>
                  <FormControl isInvalid={errors.temperature?.min}>
                    <FormLabel>Temperatura mínima</FormLabel>
                    <Input
                      variant="primary"
                      value={defaultValues.temperature.min}
                      name="temperature.min"
                      onChange={(e) =>
                        setDefaultValues({
                          ...defaultValues,
                          temperature: {
                            ...defaultValues.temperature,
                            min: e.target.value,
                          },
                        })
                      }
                      type="number"
                      placeholder="Temp. °C"
                      size="md"
                    />
                    <FormErrorMessage>
                      {errors.temperature?.min?.message}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={errors.speed?.min}>
                    <FormLabel>Velocidade mínima do vento</FormLabel>
                    <Input
                      variant="primary"
                      name="speed.min"
                      value={defaultValues.speed.min}
                      onChange={(e) =>
                        setDefaultValues({
                          ...defaultValues,
                          speed: {
                            ...defaultValues.speed,
                            min: e.target.value,
                          },
                        })
                      }
                      type="number"
                      placeholder="Vel. km/h"
                    />
                    <FormErrorMessage>
                      {errors.speed?.min?.message}
                    </FormErrorMessage>
                  </FormControl>
                </VStack>
                <VStack w={['100%', '50%']}>
                  <Box w="100%" my={4}>
                    <Text>Umidade máxima do solo</Text>
                    <Controller
                      control={control}
                      initialValue={defaultValues.humidadeSoloMax}
                      name="humidadeSoloMax"
                      render={({ field }) => (
                        <div>
                          <SliderThumbWithTooltip
                            name="humidadeSoloMax"
                            value={defaultValues.humidadeSoloMax}
                            setValue={(value) => {
                              field.onChange(value);
                              setDefaultValues({
                                ...defaultValues,
                                humidadeSoloMax: value,
                              });
                            }}
                          />
                          {validateHumidadeSolo(
                            defaultValues.humidadeSoloMax
                          ) && (
                            <div style={{ marginTop: '10px' }}>
                              <span
                                style={{
                                  color: '#e53e3e',
                                  fontSize: '0.875rem',
                                }}
                              >
                                Umidade do solo máxima não pode ser menor que a
                                mínima
                              </span>
                            </div>
                          )}
                        </div>
                      )}
                    />
                  </Box>
                  <Box w="100%" my={4}>
                    <Text>Umidade máxima do ar</Text>
                    <Controller
                      control={control}
                      name="humidadeArMax"
                      render={({ field }) => (
                        <div>
                          <SliderThumbWithTooltip
                            name="humidadeArMax"
                            value={defaultValues.humidadeArMax}
                            setValue={(value) => {
                              field.onChange(value);
                              setDefaultValues({
                                ...defaultValues,
                                humidadeArMax: value,
                              });
                            }}
                          />
                          {validateHumidadeAr(defaultValues.humidadeArMax) && (
                            <div style={{ marginTop: '10px' }}>
                              <span
                                style={{
                                  color: '#e53e3e',
                                  fontSize: '0.875rem',
                                }}
                              >
                                Umidade do ar máxima não pode ser menor que a
                                mínima
                              </span>
                            </div>
                          )}
                        </div>
                      )}
                    />
                  </Box>
                  <FormControl isInvalid={errors.temperature?.max}>
                    <FormLabel>Temperatura máxima</FormLabel>
                    <Input
                      variant="primary"
                      value={defaultValues.temperature.max}
                      onChange={(e) =>
                        setDefaultValues({
                          ...defaultValues,
                          temperature: {
                            ...defaultValues.temperature,
                            max: e.target.value,
                          },
                        })
                      }
                      type="number"
                      placeholder="Temp. °C"
                      size="md"
                    />
                    {validateTemperature(defaultValues.temperature.max) && (
                      <div style={{ marginTop: '10px' }}>
                        <span
                          style={{
                            color: '#e53e3e',
                            fontSize: '0.875rem',
                          }}
                        >
                          Temperatura máxima não pode ser menor que a mínima
                        </span>
                      </div>
                    )}
                    <FormErrorMessage />
                  </FormControl>
                  <FormControl isInvalid={errors.speed?.max}>
                    <FormLabel>Velocidade máxima do vento</FormLabel>
                    <Input
                      type="number"
                      variant="primary"
                      value={defaultValues.speed.max}
                      onChange={(e) =>
                        setDefaultValues({
                          ...defaultValues,
                          speed: {
                            ...defaultValues.speed,
                            max: e.target.value,
                          },
                        })
                      }
                      placeholder="Vel. Km/h"
                    />
                    {validateSpeed(defaultValues.speed.max) && (
                      <div style={{ marginTop: '10px' }}>
                        <span
                          style={{
                            color: '#e53e3e',
                            fontSize: '0.875rem',
                          }}
                        >
                          Velocidade máxima não pode ser menor que a mínima
                        </span>
                      </div>
                    )}
                    <FormErrorMessage />
                  </FormControl>
                </VStack>
              </Stack>
              <Divider marginTop="15px" />
            </chakra.form>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              variant="primary"
              form={PRESET_FORM_ID}
              onClick={async () => {
                if (validateAll()) {
                  onUpdate(defaultValues);
                  onClose();
                } else {
                  toast({
                    title: 'Erro ao salvar',
                    description: 'Verifique os campos e tente novamente',
                    status: 'error',
                    duration: 5000,
                    isClosable: true,
                  });
                }
              }}
            >
              Salvar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
