import { useMutation, useQueryClient, useQuery } from '@tanstack/react-query';
import axios from 'axios';
import { api } from '../../../config/lib/axios';
import { PLANTACOES_CACHE_KEYS } from '../constants/cache';
import { toast } from '../../../utils/toast';

const user = JSON.parse(localStorage.getItem('userValue'));
const apiPlantacao = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

const getAllPlantacoesByUser = async (count) =>
  apiPlantacao
    .get(`/plantacao/user/${user.id}`)
    .then((response) => response.data)
    .catch((err) => {
      const errMessage =
        err?.response?.data?.message ??
        'Não foi possível carregar as plantações. Tente novamente mais tarde!';
      if (count === 1) {
        toast.error(errMessage);
      }
      return [];
    });

export const useGetAllPlantacoesByUser = (count) =>
  useQuery({
    queryKey: [PLANTACOES_CACHE_KEYS.allPlantacoes],
    queryFn: () => getAllPlantacoesByUser(count),
  });
