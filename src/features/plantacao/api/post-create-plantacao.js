import { useMutation, useQueryClient } from '@tanstack/react-query';
import axios from 'axios';
import { api } from '../../../config/lib/axios';
import { PLANTACOES_CACHE_KEYS } from '../constants/cache';
import { toast } from '../../../utils/toast';

const apiPlantacao = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

export function postCreatePlantacao(data) {
  return apiPlantacao.post(`/plantacao`, data);
}

export function usePostCreatePlantacao({ onSuccessCallBack }) {
  const queryClient = useQueryClient();
  return useMutation(postCreatePlantacao, {
    onSuccess() {
      queryClient.invalidateQueries([PLANTACOES_CACHE_KEYS.allPlantacoes]);

      toast.success('Plantação criada com sucesso!');

      onSuccessCallBack?.();
    },
    onError(error) {
      const errorMessage = Array.isArray(error?.response?.data?.message)
        ? error?.response?.data?.message[0]
        : error?.response?.data?.message;
      toast.error(
        errorMessage ?? '',
        'Houve um problema ao tentar criar a plantação.'
      );
    },
  });
}
