import { useMutation, useQueryClient } from '@tanstack/react-query';
import axios from 'axios';
import { useToast } from '@chakra-ui/react';
import { api } from '../../../config/lib/axios';
import { PLANTACOES_CACHE_KEYS } from '../constants/cache';

const apiPlantacao = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

export function putUpdatePlantacao(data) {
  return apiPlantacao.put(`/plantacao/${data.id}`, data);
}

// export function putUpdateManual(data) {
//   const command = {command: data.manual ? 'on' : 'off'};
//   return apiPlantacao.put(`${data.id}/control`, command);
// }

export const usePutUpdatePlantacao = () => {
  const queryClient = useQueryClient();

  const toast = useToast({ position: 'bottom' });
  const mutation = useMutation({
    mutationFn: putUpdatePlantacao,
    onSuccess: (data) => {
      toast({
        title: 'Irrigação ativada/desativada com sucesso.',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });

      // Limpa o cache para que os dados sejam atualizados
      queryClient.invalidateQueries(PLANTACOES_CACHE_KEYS);
    },
    onError: (err) => {
      toast({
        title: 'Houve um problema ao tentar ativar/desativar a plantação.',
        description: '',
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });

  return mutation;
};
