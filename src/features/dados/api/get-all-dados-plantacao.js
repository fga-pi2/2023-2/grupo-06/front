import { useMutation, useQueryClient, useQuery } from '@tanstack/react-query';
import axios from 'axios';
import { api } from '../../../config/lib/axios';
import { DADOS_CACHE_KEYS } from '../constants/cache';
import { toast } from '../../../utils/toast';

const apiDado = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

const getAllDadosByPlantacao = async (plantacaoId) =>
  apiDado
    .get(`/dados/plantacao/${plantacaoId}`)
    .then((response) => response.data)
    .catch((err) => {
      const errMessage =
        err?.response?.data?.message ??
        'Não foi possível carregar os dados. Tente novamente mais tarde!';
      if (errMessage && plantacaoId.length > 0) {
        // toast.error(errMessage);
      }
      return [];
    });

export const useGetAllDadosByPlantacao = (plantacaoId) =>
  useQuery({
    refetchInterval: 1000 * 5,
    queryKey: [DADOS_CACHE_KEYS.allDados, plantacaoId],
    queryFn: () => getAllDadosByPlantacao(plantacaoId),
  });
