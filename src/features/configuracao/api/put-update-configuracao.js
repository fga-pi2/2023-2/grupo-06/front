import { useMutation, useQueryClient } from '@tanstack/react-query';
import axios from 'axios';
import { useToast } from '@chakra-ui/react';
import { api } from '../../../config/lib/axios';
import { CONFIGURACOES_CACHE_KEYS } from '../constants/cache';

const apiConfiguracao = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

export function putUpdateConfiguracao(data) {
  return apiConfiguracao.put(`/configuracao/${data.id}`, data);
}

export const usePutUpdateConfiguracao = () => {
  const queryClient = useQueryClient();

  const toast = useToast({ position: 'bottom' });
  const mutation = useMutation({
    mutationFn: putUpdateConfiguracao,
    onSuccess: (data) => {
      toast({
        title: 'Configuração alterada com sucesso.',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });

      // Limpa o cache para que os dados sejam atualizados
      queryClient.invalidateQueries(CONFIGURACOES_CACHE_KEYS);
    },
    onError: (err) => {
      toast({
        title: 'Houve um problema ao tentar alterar a configuração.',
        description: '',
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });

  return mutation;
};
