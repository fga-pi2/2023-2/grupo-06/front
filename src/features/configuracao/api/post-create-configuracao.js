import { useMutation, useQueryClient } from '@tanstack/react-query';
import axios from 'axios';
import { api } from '../../../config/lib/axios';
import { CONFIGURACOES_CACHE_KEYS } from '../constants/cache';
import { toast } from '../../../utils/toast';

const apiConfiguracao = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

export function postCreateConfiguracao(data) {
  return apiConfiguracao.post(`/configuracao`, data);
}

export function usePostCreateConfiguracao({ onSuccessCallBack }) {
  const queryClient = useQueryClient();

  return useMutation(postCreateConfiguracao, {
    onSuccess() {
      queryClient.invalidateQueries([CONFIGURACOES_CACHE_KEYS.allPreset]);
      toast.success('Configuração criada com sucesso!');

      onSuccessCallBack?.();
    },
    onError(error) {
      const errorMessage = Array.isArray(error?.response?.data?.message)
        ? error?.response?.data?.message[0]
        : error?.response?.data?.message;
      toast.error(
        errorMessage ?? '',
        'Houve um problema ao tentar criar a configuração.'
      );
    },
  });
}
