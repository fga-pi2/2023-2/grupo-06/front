import React, { useState } from 'react';
import {
  chakra,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Text,
  Stack,
  Box,
  Input,
  Button,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  Tooltip,
  Divider,
  FormControl,
  FormLabel,
  FormErrorMessage,
  ModalFooter,
  VStack,
  useDisclosure,
  Center,
} from '@chakra-ui/react';
import { useForm, Controller } from 'react-hook-form';
import { AddIcon } from '@chakra-ui/icons';

function SliderThumbWithTooltip({ name, value, setValue }) {
  const [showTooltip, setShowTooltip] = useState(false);

  return (
    <Slider
      id={`slider-${name}`}
      value={value}
      min={0}
      max={100}
      colorScheme="green"
      onChange={(v) => setValue(v)}
      onMouseEnter={() => setShowTooltip(true)}
      onMouseLeave={() => setShowTooltip(false)}
    >
      <SliderMark value={5} mt="1" fontSize="sm">
        0%
      </SliderMark>
      <SliderMark value={93} mt="1" fontSize="sm">
        100%
      </SliderMark>
      <SliderTrack>
        <SliderFilledTrack />
      </SliderTrack>
      <Tooltip
        hasArrow
        bg="teal.500"
        color="white"
        placement="top"
        isOpen={showTooltip}
        label={`${value}%`}
      >
        <SliderThumb />
      </Tooltip>
    </Slider>
  );
}

const PRESET_FORM_ID = 'preset-form';

export function CadastroPlantacaoModal({ onSubmitPreset }) {
  const {
    control,
    register,
    handleSubmit,
    watch,
    trigger,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: '',
      autoIrrigate: false,
      temperature: {
        min: '',
        max: '',
      },
      speed: {
        min: '',
        max: '',
      },
      humidadeSoloMin: 0,
      humidadeSoloMax: 0,
      humidadeArMin: 0,
      humidadeArMax: 0,
    },
  });
  const { isOpen, onOpen, onClose } = useDisclosure();

  const temperaturaMin = watch('temperature.min');
  const velocidadeMin = watch('speed.min');
  const humidadeSoloMin = watch('humidadeSoloMin');
  const humidadeArMin = watch('humidadeArMin');

  return (
    <>
      <Center>
        <Tooltip
          label="Definir Nova Configuração"
          aria-label="Definir Nova Configuração"
        >
          <Button
            mb="2vh"
            variant="secondary"
            leftIcon={<AddIcon />}
            onClick={onOpen}
          >
            Criar Configuração
          </Button>
        </Tooltip>
      </Center>
      <Modal size="xl" isOpen={isOpen} onClose={onClose}>
        <ModalOverlay backdropFilter="blur(3px)" />
        <ModalContent bg="#0A7176">
          <ModalHeader textAlign="center" fontSize="2xl">
            Criar Configuração
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <chakra.form
              id={PRESET_FORM_ID}
              onSubmit={handleSubmit(onSubmitPreset)}
            >
              <FormControl isInvalid={errors.name}>
                <FormLabel>Nome da configuração</FormLabel>
                <Input
                  variant="primary"
                  {...register('name', {
                    required: 'Necessário inserir um nome',
                  })}
                  placeholder="Digite o nome da configuração"
                />
                <FormErrorMessage>{errors.name?.message}</FormErrorMessage>
              </FormControl>
              <Divider marginTop="15px" />
              <Stack direction={['column', 'row']} spacing={8} mb={2}>
                {/* Conteúdo da modal */}
                <VStack w={['100%', '50%']} alignContent="stretch">
                  <Box w="100%" my={4} marginBottom="5px">
                    <Text>Umidade mínima do solo</Text>
                    <Controller
                      control={control}
                      name="humidadeSoloMin"
                      defaultValue={0}
                      rules={{ required: 'Este campo é obrigatório' }}
                      render={({ field }) => (
                        <SliderThumbWithTooltip
                          name="humidadeSoloMin"
                          value={field.value}
                          setValue={field.onChange}
                        />
                      )}
                    />
                  </Box>
                  <Box w="100%" my={4} marginBottom="5px">
                    <Text>Umidade mínima do ar</Text>
                    <Controller
                      control={control}
                      name="humidadeArMin"
                      defaultValue={0}
                      rules={{ required: 'Este campo é obrigatório' }}
                      render={({ field }) => (
                        <SliderThumbWithTooltip
                          name="humidadeArMin"
                          value={field.value}
                          setValue={field.onChange}
                        />
                      )}
                    />
                  </Box>
                  <FormControl isInvalid={errors.temperature?.min}>
                    <FormLabel>Temperatura mínima</FormLabel>
                    <Input
                      variant="primary"
                      {...register('temperature.min', {
                        required: 'Este campo é obrigatório',
                      })}
                      type="number"
                      placeholder="Temp. °C"
                      size="md"
                    />
                    <FormErrorMessage>
                      {errors.temperature?.min?.message}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={errors.speed?.min}>
                    <FormLabel>Velocidade mínima do vento</FormLabel>
                    <Input
                      variant="primary"
                      {...register('speed.min', {
                        required: 'Este campo é obrigatório',
                      })}
                      type="number"
                      placeholder="Vel. km/h"
                    />
                    <FormErrorMessage>
                      {errors.speed?.min?.message}
                    </FormErrorMessage>
                  </FormControl>
                </VStack>
                <VStack w={['100%', '50%']}>
                  <Box w="100%" my={4} marginBottom="5px">
                    <Text>Umidade máxima do solo</Text>
                    <Controller
                      control={control}
                      name="humidadeSoloMax"
                      defaultValue={0}
                      rules={{
                        required: 'Este campo é obrigatório',
                        validate: (value) => {
                          return (
                            Number(value) >= Number(humidadeSoloMin) ||
                            'Umidade máxima do solo não pode ser menor que a mínima'
                          );
                        },
                      }}
                      render={({ field }) => (
                        <div>
                          <SliderThumbWithTooltip
                            name="humidadeSoloMax"
                            value={field.value}
                            setValue={field.onChange}
                          />
                          {field.value < Number(humidadeSoloMin) && (
                            <div style={{ marginTop: '10px' }}>
                              <span
                                style={{
                                  color: '#e53e3e',
                                  fontSize: '0.875rem',
                                }}
                              >
                                Umidade máxima do solo não pode ser menor que a
                                mínima
                              </span>
                            </div>
                          )}
                        </div>
                      )}
                    />
                  </Box>
                  <Box w="100%" my={4} marginBottom="5px">
                    <Text>Umidade máxima do ar</Text>
                    <Controller
                      control={control}
                      name="humidadeArMax"
                      defaultValue={0}
                      rules={{
                        required: 'Este campo é obrigatório',
                        validate: (value) => {
                          return (
                            Number(value) >= Number(humidadeArMin) ||
                            'Umidade máxima do ar não pode ser menor que a mínima'
                          );
                        },
                      }}
                      render={({ field }) => (
                        <div>
                          <SliderThumbWithTooltip
                            name="humidadeArMax"
                            value={field.value}
                            setValue={field.onChange}
                          />
                          {field.value < Number(humidadeArMin) && (
                            <div style={{ marginTop: '10px' }}>
                              <span
                                style={{
                                  color: '#e53e3e',
                                  fontSize: '0.875rem',
                                }}
                              >
                                Umidade máxima do ar não pode ser menor que a
                                mínima
                              </span>
                            </div>
                          )}
                        </div>
                      )}
                    />
                  </Box>
                  <FormControl isInvalid={errors.temperature?.max}>
                    <FormLabel>Temperatura máxima</FormLabel>
                    <Input
                      type="number"
                      variant="primary"
                      {...register('temperature.max', {
                        required: 'Este campo é obrigatório',
                        validate: (value) => {
                          return (
                            Number(value) >= Number(temperaturaMin) ||
                            'Temperatura máxima não pode ser menor que a mínima'
                          );
                        },
                      })}
                      placeholder="Temp. °C"
                    />
                    <FormErrorMessage>
                      {errors.temperature?.max?.message}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl isInvalid={errors.speed?.max}>
                    <FormLabel>Velocidade máxima do vento</FormLabel>
                    <Input
                      type="number"
                      variant="primary"
                      {...register('speed.max', {
                        required: 'Este campo é obrigatório',
                        validate: (value) => {
                          return (
                            Number(value) >= Number(velocidadeMin) ||
                            'Velocidade máxima do vento não pode ser menor que a mínima'
                          );
                        },
                      })}
                      placeholder="Vel. Km/h"
                    />
                    <FormErrorMessage>
                      {errors.speed?.max?.message}
                    </FormErrorMessage>
                  </FormControl>
                </VStack>
              </Stack>
              <Divider marginTop="15px" />
            </chakra.form>
          </ModalBody>
          <ModalFooter justifyContent="center">
            <Button
              variant="primary"
              form={PRESET_FORM_ID}
              onClick={async () => {
                const isValid = await trigger();
                if (isValid) {
                  onClose();
                  handleSubmit(onSubmitPreset)();
                }
              }}
            >
              Criar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
