export const PRESETS_CACHE_KEYS = {
  allPresets: 'all-presets-cache',
  preset: 'preset-cache',
};
