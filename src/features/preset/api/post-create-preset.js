import { useMutation, useQueryClient } from '@tanstack/react-query';
import axios from 'axios';
import { api } from '../../../config/lib/axios';
import { PRESETS_CACHE_KEYS } from '../constants/cache';
import { toast } from '../../../utils/toast';

const apiPreset = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

export function postCreatePreset(data) {
  return apiPreset.post(`/preset`, data);
}

export function usePostCreatePreset({ onSuccessCallBack }) {
  const queryClient = useQueryClient();

  return useMutation(postCreatePreset, {
    onSuccess() {
      queryClient.invalidateQueries([PRESETS_CACHE_KEYS.allPreset]);
      toast.success('Preset criada com sucesso!');

      onSuccessCallBack?.();
    },
    onError(error) {
      const errorMessage = Array.isArray(error?.response?.data?.message)
        ? error?.response?.data?.message[0]
        : error?.response?.data?.message;
      toast.error(
        errorMessage ?? '',
        'Houve um problema ao tentar criar o preset.'
      );
    },
  });
}
