import { useQuery } from '@tanstack/react-query';
import axios from 'axios';
import { toast } from '../../../utils/toast';
import { api } from '../../../config/lib/axios';
import { PRESETS_CACHE_KEYS } from '../constants/cache';
import { PRESETS_ENDPOINT } from '../constants/requests';

const apiPreset = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformRequest: [...axios.defaults.transformRequest],
});

const getAllPresets = async (count) =>
  apiPreset
    .get(`/preset`)
    .then((response) => response.data)
    .catch((err) => {
      console.log(PRESETS_ENDPOINT);
      const errMessage =
        err?.response?.data?.message ??
        'Não foi possível carregar os presets. Tente novamente mais tarde!';
      if (count === 1) {
        toast.error(errMessage);
      }
      return [];
    });

const getPreset = async (presetId) => {
  try {
    const response = await apiPreset.get(`/preset/${presetId}`);
    return response.data;
  } catch (err) {
    toast.error(
      'Não foi possível carregar o preset. Tente novamente mais tarde!'
    );
    return null;
  }
};

export const useGetAllPresets = (count) =>
  useQuery({
    queryKey: [PRESETS_CACHE_KEYS.allPresets],
    queryFn: () => getAllPresets(count),
  });

export const useGetPreset = (presetId) =>
  useQuery({
    queryKey: [PRESETS_CACHE_KEYS.preset],
    queryFn: () => getPreset(presetId),
  });
