import React from 'react';
import { render, screen } from '@testing-library/react';
import Sidebar from './sideBar';
import { Wrapper } from '../utils/testWrapper';

describe('Sidebar', () => {
  it('renders the sidebar with the correct elements', () => {
    render(
      <Wrapper>
        <Sidebar />
      </Wrapper>
    );

    const perfil = screen.getByText('Perfil');
    expect(perfil).toBeInTheDocument();

    const plantacao = screen.getByText('Plantações');
    expect(plantacao).toBeInTheDocument();

    const novaPlantacao = screen.getByText('Nova Plantação');
    expect(novaPlantacao).toBeInTheDocument();
  });
  it('renders the sidebar buttons correctly', () => {
    render(
      <Wrapper>
        <Sidebar />
      </Wrapper>
    );

    const perfilButton = screen.getByRole('button', { name: 'Perfil' });
    expect(perfilButton).toBeInTheDocument();

    const plantacaoButton = screen.getByRole('button', { name: 'Plantações' });
    expect(plantacaoButton).toBeInTheDocument();

    const novaPlantacaoButton = screen.getByRole('button', {
      name: 'Nova Plantação',
    });
    expect(novaPlantacaoButton).toBeInTheDocument();
  });
});
