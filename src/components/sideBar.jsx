import React from 'react';
import {
  Box,
  Icon,
  Button,
  Flex,
  Stack,
  Image,
  Spacer,
  Center,
} from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { PiPlantFill } from 'react-icons/pi';
import { BiSolidUserCircle } from 'react-icons/bi';
import { FiLogOut } from 'react-icons/fi';
import { BsBellFill } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';

const property = {
  imageUrl: '/irrigaja_logo.png',
  imageAlt: 'Exemplo',
};

export default function Sidebar() {
  const navigate = useNavigate();

  return (
    <Box w="300px" h="100vh" p="10" color="white" borderRight="1px" maxW="sm">
      <Stack direction={['column', 'row']} mb="5vh" alignContent="flex">
        <Center>
          <Icon
            as={BsBellFill}
            boxSize="5"
            ml="1"
            mt="2"
            cursor="pointer" // Adiciona uma seta de cursor para indicar que é clicável
            onClick={() => navigate('/perfil')} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
          />
        </Center>
        <Image
          boxSize="85%"
          fit="cover"
          src={property.imageUrl}
          alt={property.imageAlt}
        />
      </Stack>

      <Flex flexDirection="column" height="90%">
        <Button
          mb="2vh"
          variant="secondary"
          leftIcon={<BiSolidUserCircle />}
          onClick={() => navigate('/perfil')} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
        >
          Perfil
        </Button>
        <Button
          mb="2vh"
          variant="secondary"
          leftIcon={<PiPlantFill />}
          onClick={() => navigate('/plantacao')} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
        >
          Plantações
        </Button>
        <Button
          mb="2vh"
          variant="secondary"
          leftIcon={<AddIcon />}
          onClick={() => navigate('/cadastroPlantacao')} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
        >
          Nova Plantação
        </Button>
        <Spacer />
        {/* <Box mb="45vh"/> */}
        <Button
          mb="2vh"
          variant="secondary"
          leftIcon={<FiLogOut />}
          onClick={() => {
            localStorage.removeItem('userValue');
            navigate('/login');
          }} // É necessário colocar o caminho correto quando for desenvolvido a tela de cadastro
        >
          Sair
        </Button>
      </Flex>
    </Box>
  );
}
