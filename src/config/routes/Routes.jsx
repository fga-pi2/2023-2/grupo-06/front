import { Routes, Route, Navigate } from 'react-router-dom';
import { SignInPage } from '../../pages/login/index';
import { SignUpPage } from '../../pages/cadastro';
import { AuthLayout } from '../../layouts/AuthLayout';
import { BaseLayout } from '../../layouts/BaseLayout';
import { Inicio } from '../../pages/inicio/inicio';
import Plantacao from '../../pages/plantacao';
import { Profile } from '../../pages/perfil';
import CadastroPlantacao from '../../pages/cadastroPlantacao';

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: SignInPage,
  },
  {
    path: '/cadastroUsuario',
    name: 'Sign Up',
    component: SignUpPage,
  },
  {
    path: '/inicio',
    name: 'Inicio',
    component: Inicio,
    exact: true,
  },
  {
    path: '/perfil',
    name: 'Perfil',
    component: Profile,
    exact: true,
  },
  {
    path: '/plantacao',
    name: 'Plantacao',
    component: Plantacao,
    exact: true,
  },
  {
    path: '/cadastroPlantacao',
    name: 'CadastroPlantacao',
    component: CadastroPlantacao,
    exact: true,
  },
];

export function Router() {
  function getLayout(route) {
    if (['/login', '/cadastroUsuario', '/inicio'].includes(route)) {
      return BaseLayout;
    }
    return AuthLayout;
  }

  function wrapComponentInLayout(route) {
    const Layout = getLayout(route.path);
    return (
      <Layout title={route.title}>
        <route.component />
      </Layout>
    );
  }

  const auth = localStorage.getItem('userValue');

  return (
    <Routes>
      {auth ? (
        <>
          {routes.map((route) => (
            <Route
              key={route.name}
              path={route.path}
              exact={route.exact}
              element={wrapComponentInLayout(route)}
            />
          ))}
          <Route path="*" element={<Navigate to="/plantacao" />} />
        </>
      ) : (
        <>
          <Route
            key="login"
            path="*"
            element={wrapComponentInLayout(routes[0])}
          />
          <Route
            key="signup"
            path="/cadastroUsuario"
            element={wrapComponentInLayout(routes[1])}
          />
        </>
      )}
      {/* <Route path="*" element={<Navigate to="/login" />} /> */}
    </Routes>
  );
}
