import { useState } from 'react';
import {
  Button,
  Center,
  Box,
  Input,
  Text,
  Grid,
  Image,
  Stack,
  Container,
  Select,
  Switch,
  Flex,
  Spacer,
  useToast,
  Tag,
  TagLabel,
  TagCloseButton,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { useGetAllPresets } from '../../features/preset/api/get-all-presets';
import { postCreatePlantacao } from '../../features/plantacao/api/post-create-plantacao';
import { postCreateConfiguracao } from '../../features/configuracao/api/post-create-configuracao';
import { CadastroPlantacaoModal } from '../../features/preset/components';

export default function CadastroPlantacao({ submit }) {
  const [isLoading, setIsLoading] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showPasswordFields, setShowPasswordFields] = useState(false);
  const [showTagFields, setShowTagFields] = useState(false);
  const navigate = useNavigate();
  // Retorna os dados do usuário logado
  const user = JSON.parse(localStorage.getItem('userValue'));

  // Cria um objeto com os dados da configuração
  const configuracaoDados = {
    nome: '',
    temperatura_min: 0,
    temperatura_max: 0,
    velocidade_ar_min: 0,
    velocidade_ar_max: 0,
    umidade_solo_min: 0,
    umidade_solo_max: 0,
    umidade_ar_min: 0,
    umidade_ar_max: 0,
  };
  const [configuracao, setConfiguracao] = useState(configuracaoDados);

  // Seleciona o preset
  const { data: presets, refetch } = useGetAllPresets();
  const [presetSelecionado, setPresetSelecionado] = useState(null);
  const handlePresetSelect = (event) => {
    const valorSelecionado = event.target.value;
    setPresetSelecionado(
      presets.find((preset) => preset.id === valorSelecionado)
    );
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // Faz o post da configuração
  async function enviaConfiguracao(plantacaoId) {
    if (!showPasswordFields) {
      console.log('configuracao', configuracao);
      configuracao.plantacao_id = plantacaoId;
      try {
        await postCreateConfiguracao(configuracao);
        // onClose();
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log('presetSelecionado', presetSelecionado);
      presetSelecionado.plantacao_id = plantacaoId;
      try {
        await postCreateConfiguracao(presetSelecionado);
      } catch (error) {
        console.log(error);
      }
    }
  }

  const toast = useToast({ position: 'bottom' });
  const mutation = useMutation({
    mutationFn: postCreatePlantacao,
    onSuccess: (data) => {
      enviaConfiguracao(data.data.id);
      toast({
        title: 'Plantação criada com sucesso.',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });
      setTimeout(() => {
        navigate('/plantacao');
      }, 1500);
    },
    onError: (err) => {
      toast({
        title: 'Não foi possível criar a plantação.',
        description: 'ID já cadastrado.',
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });

  const { isPending: isloading } = mutation;
  const onSubmit = async ({ name, id }) => {
    console.log('config', configuracao);
    console.log('preset', presetSelecionado);
    if (configuracao.nome === '' && presetSelecionado === null) {
      toast({
        title: 'Você precisa criar uma configuração ou selecionar um Preset',
        description: '',
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    } else {
      await mutation.mutate({
        nome: name,
        id,
        manual: false,
        status: false,
        user_email: user.email,
      });
    }
  };

  const togglePasswordFields = () => {
    setShowPasswordFields(!showPasswordFields);
    setPresetSelecionado(null);
  };

  const property = {
    imageUrl: '/simple-logo.png',
    imageAlt: 'Exemplo',
  };

  // Adiciona os dados da configuração ao objeto configuracaoDados
  const onSubmitPreset = ({
    name,
    temperature,
    speed,
    humidadeArMin,
    humidadeArMax,
    humidadeSoloMin,
    humidadeSoloMax,
  }) => {
    configuracaoDados.nome = name;
    configuracaoDados.temperatura_min = temperature.min;
    configuracaoDados.temperatura_max = temperature.max;
    configuracaoDados.velocidade_ar_min = speed.min;
    configuracaoDados.velocidade_ar_max = speed.max;
    configuracaoDados.umidade_solo_min = humidadeSoloMin;
    configuracaoDados.umidade_solo_max = humidadeSoloMax;
    configuracaoDados.umidade_ar_min = humidadeArMin;
    configuracaoDados.umidade_ar_max = humidadeArMax;
    console.log('configuracaoDados', configuracaoDados);
    setConfiguracao(configuracaoDados);
    setShowTagFields(name);
    toast({
      title: 'Configuração criada com sucesso.',
      description: '',
      status: 'success',
      duration: 1500,
      isClosable: true,
    });
  };

  return (
    <Stack direction="row" spacing={0}>
      <Container centerContent maxW="md">
        <Stack textAlign="center">
          <Box display="flex" justifyContent="center">
            <Image
              width="20%"
              fit="cover"
              src={property.imageUrl}
              alt={property.imageAlt}
            />
          </Box>
          <Text fontSize="4xl">Cadastro de Plantação</Text>
        </Stack>
        <Center
          aria-label="form"
          backgroundColor="#0D4545"
          h="75vh"
          color="white"
        >
          <form onSubmit={handleSubmit(onSubmit)} aria-label="form">
            <Box color="white" w="23vw">
              <Box>
                <Text color="white" fontSize="lg">
                  Nome da plantação
                </Text>

                <Input
                  size="md"
                  // mb= "2.5vh"
                  fontSize="md"
                  variant="primary"
                  {...register('name', { required: true })}
                  placeholder="Nome da plantação"
                />
                {errors.name && (
                  <span>
                    <Text color="red.400">Este campo é obrigatório</Text>
                  </span>
                )}
                <Box mb="2.5vh" />
                <Text color="white" fontSize="lg">
                  {' '}
                  ID do produto{' '}
                </Text>
                <Input
                  size="md"
                  // mb= "2.5vh"
                  fontSize="md"
                  variant="primary"
                  {...register('id', { required: true })}
                  placeholder="ID do produto"
                />
                {errors.id && (
                  <span>
                    <Text color="red.400">Este campo é obrigatório</Text>
                  </span>
                )}
                <Box mb="2.5vh" />

                <Flex>
                  <Text color="white" fontSize="lg" mb="1vh">
                    Usar configuração existente
                  </Text>
                  <Spacer />
                  <Switch
                    alignContent="end"
                    colorScheme="green"
                    mb="2.5vh"
                    onChange={togglePasswordFields}
                    isChecked={showPasswordFields}
                  />
                </Flex>

                {!showPasswordFields && ( // Renderiza CadastroPlantacaoModal se o Switch estiver desativado
                  <>
                    <CadastroPlantacaoModal onSubmitPreset={onSubmitPreset} />
                    {showTagFields && (
                      <Center>
                        <Tag
                          size="lg"
                          borderRadius="full"
                          variant="solid"
                          colorScheme="green"
                        >
                          <TagLabel>{showTagFields}</TagLabel>
                          <TagCloseButton
                            onClick={() => {
                              setShowTagFields(false);
                              setConfiguracao(configuracaoDados);
                            }}
                          />
                        </Tag>
                      </Center>
                    )}
                  </>
                )}
                {showPasswordFields && ( // Renderiza Select de Preset se o Switch estiver ativado
                  <div>
                    <Text color="white" fontSize="lg">
                      {' '}
                      Configuração{' '}
                    </Text>
                    <Select
                      color="gray"
                      background="white"
                      {...register('preset', { required: true })}
                      placeholder="Selecione uma configuração"
                      onClick={refetch}
                      onChange={handlePresetSelect}
                    >
                      {presets &&
                        presets.map((preset) => (
                          <option key={preset.id} value={preset.id}>
                            {preset.nome}
                          </option>
                        ))}
                    </Select>
                    {errors.preset && (
                      <span>
                        <Text color="red.400">Este campo é obrigatório</Text>
                      </span>
                    )}
                  </div>
                )}
                <Box mb="10vh" />
                <Grid>
                  <Button variant="primary" type="submit" isLoading={isLoading}>
                    Criar plantação
                  </Button>
                </Grid>
              </Box>
            </Box>
          </form>
        </Center>
      </Container>
    </Stack>
  );
}
