import { render, screen } from '@testing-library/react';
import CadastroPlantacao from './index';
import { Wrapper } from '../../utils/testWrapper';

describe('Cadastro de Plantação', () => {
  it('renders the title', () => {
    render(
      <Wrapper>
        <CadastroPlantacao />
      </Wrapper>
    );
    const title = screen.getByText('Cadastro de Plantação');
    expect(title).toBeInTheDocument();
  });
});
