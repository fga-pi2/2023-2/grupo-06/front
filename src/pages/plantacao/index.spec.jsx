import { render, screen } from '@testing-library/react';
import Plantacao from './index';
import { Wrapper } from '../../utils/testWrapper';

describe('Plantacao', () => {
  it('renders the title', () => {
    render(
      <Wrapper>
        <Plantacao />
      </Wrapper>
    );
    const title = screen.getByText('Temperatura (°C)');
    expect(title).toBeInTheDocument();
  });
  it('render the buttons', () => {
    render(
      <Wrapper>
        <Plantacao />
      </Wrapper>
    );
    const parametrosButton = screen.getByRole('button', {
      name: 'Editar configuração',
    });
    expect(parametrosButton).toBeInTheDocument();

    const ativarManualButton = screen.getByRole('button', {
      name: 'Ativar manualmente',
    });
    expect(ativarManualButton).toBeInTheDocument();
  });
});
