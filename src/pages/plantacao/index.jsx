import { useState, useEffect } from 'react';
import {
  Button,
  Center,
  Box,
  Text,
  Grid,
  Select,
  Card,
  CardHeader,
  Heading,
  Stack,
  Divider,
  GridItem,
  Tooltip as Tip,
  Flex,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { WarningTwoIcon } from '@chakra-ui/icons';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { useGetAllPlantacoesByUser } from '../../features/plantacao/api/get-all-plantacoes-user';
import { usePutUpdatePlantacao } from '../../features/plantacao/api/put-update-plantacao';
import { CadastroPlantacaoModal } from '../../features/plantacao/components';
import { useGetAllDadosByPlantacao } from '../../features/dados/api/get-all-dados-plantacao';

const user = JSON.parse(localStorage.getItem('userValue'));
let labels = [];
let listaTemperatura = [];
let listaUmidadeSolo = [];
let listaUmidadeAr = [];
let listaVelocidadeAr = [];

let ultimaTemperatura;
let ultimaUmidadeAr;
let ultimaVelocidadeAr;

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: true,
    },
  },
};

export default function Plantacao() {
  // const { signIn } = useAuth();
  const [isLoading, setIsLoading] = useState(false);
  const { data: plantacoes } = useGetAllPlantacoesByUser();

  // const toast = useToast({ position: 'bottom' });

  const [objetoPlantacao, setObjetoPlantacao] = useState({
    id: '',
    nome: '',
    manual: '',
    user: {
      id: '',
    },
    configuracao: [],
  });

  const [objetoDados, setobjetoDados] = useState([
    {
      dados: [],
    },
  ]);

  const { data: dadosPlantacao, refetch } = useGetAllDadosByPlantacao(
    objetoPlantacao?.id || ''
  );
  const [isUpdating, setIsUpdating] = useState(objetoPlantacao.manual);

  useEffect(() => {
    if (dadosPlantacao) {
      const sortedDados =
        dadosPlantacao.length > 0
          ? [...dadosPlantacao].sort(
              (a, b) => new Date(a.registro) - new Date(b.registro)
            )
          : [];

      setobjetoDados({
        dados: sortedDados,
      });

      labels = sortedDados.map((dado) => {
        const data = new Date(dado.registro);
        // data.setHours(data.getHours() + 3);
        const tempoFormatado = data.toLocaleTimeString('pt-BR', {
          hour: '2-digit',
          minute: '2-digit',
          second: '2-digit',
        });
        return `${tempoFormatado}`;
      });

      listaTemperatura = sortedDados.map((dado) => dado.temperatura);
      listaUmidadeSolo = sortedDados.map((dado) => dado.umidade_solo);
      listaUmidadeAr = sortedDados.map((dado) => dado.umidade_ar);
      listaVelocidadeAr = sortedDados.map((dado) => dado.velocidade_ar);

      // if (!listaTemperatura) {
      //   ultimaTemperatura = listaTemperatura[listaTemperatura.length - 1];

      //   if (
      //     ultimaTemperatura < objetoPlantacao.configuracao.temperatura_min ||
      //     ultimaTemperatura > objetoPlantacao.configuracao.temperatura_min
      //   ) {
      //     toast({
      //       title: 'Temperatura fora dos parâmetros definidos.',
      //       description: '',
      //       status: 'warning',
      //       duration: 6000,
      //       isClosable: true,
      //     });
      //   }
      // }
    }
  }, [
    dadosPlantacao,
    isUpdating,
    objetoPlantacao.configuracao.temperatura_min,
    // toast,
  ]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const mutation = usePutUpdatePlantacao();

  const onSubmit = async () => {
    mutation.mutate({
      id: objetoPlantacao.id,
      manual: !isUpdating,
    });
    setIsUpdating(!isUpdating);
  };

  useEffect(() => {
    setIsUpdating(objetoPlantacao.manual);
  }, [objetoPlantacao.manual]);

  useEffect(() => {
    refetch();
  }, [objetoDados.dados]);

  const handlePlantacaoSelect = async (event) => {
    const selectedPlantacaoId = event.target.value;
    const selectedPlantacao = plantacoes.find(
      (plantacao) => plantacao.id === selectedPlantacaoId
    );
    if (selectedPlantacao) {
      setObjetoPlantacao({
        id: selectedPlantacao.id,
        nome: selectedPlantacao.nome,
        manual: selectedPlantacao.manual,
        user: {
          id: selectedPlantacao.user.id,
        },
        configuracao: selectedPlantacao.configuracao,
      });
    }
  };

  const createChartData = (nome, tipo, bola, linha) => {
    return {
      labels,
      datasets: [
        {
          label: nome,
          data: tipo,
          backgroundColor: bola, // bola
          borderColor: linha, // linha
        },
      ],
    };
  };

  return (
    <Stack direction="row" spacing={0}>
      <Box flex="1" style={{ margin: '0 5vw' }}>
        <Text fontSize="3xl">
          {objetoDados && objetoDados.dados && objetoDados.dados.length > 0
            ? objetoPlantacao.nome
            : 'Selecione uma plantação'}
        </Text>
        <Divider />
        <Box mb="2vh" />
        <Grid templateColumns="2fr 2fr 6fr" gap={1} mb="3vh">
          <CadastroPlantacaoModal isEditing={objetoPlantacao.configuracao} />
          {isUpdating ? (
            <Button
              fontWeight="normalbold"
              backgroundColor="#B22222"
              type="button"
              maxW="12vw"
              maxH="5vh"
              onClick={handleSubmit(onSubmit)}
              isLoading={isLoading}
            >
              Desativar manualmente
            </Button>
          ) : (
            <Button
              fontWeight="normalbold"
              variant="primary"
              type="button"
              maxW="12vw"
              maxH="5vh"
              onClick={handleSubmit(onSubmit)}
              isLoading={isLoading}
            >
              Ativar manualmente
            </Button>
          )}
          <Select
            color="gray"
            background="white"
            maxH="5vh"
            {...register('plantacao', { required: true })}
            placeholder="Selecione uma plantação"
            onClick={handlePlantacaoSelect}
          >
            {plantacoes &&
              plantacoes
                .filter((plantacao) => plantacao.user.id === user.id)
                .map((plantacao) => (
                  <option key={plantacao.id} value={plantacao.id}>
                    {plantacao.nome}
                  </option>
                ))}
          </Select>
        </Grid>
        <Grid templateColumns="repeat(4, 1fr)" gap={4} mb="2vh">
          <GridItem>
            <Card>
              <CardHeader>
                <Center>
                  <Heading size="sm">Temperatura (°C)</Heading>
                  <Text marginLeft={1}>
                    {objetoDados &&
                    objetoDados.dados &&
                    objetoPlantacao.configuracao &&
                    objetoDados.dados.length > 0 &&
                    (Number(
                      objetoDados.dados[objetoDados.dados.length - 1]
                        .temperatura
                    ) < objetoPlantacao.configuracao.temperatura_min ||
                      Number(
                        objetoDados.dados[objetoDados.dados.length - 1]
                          .temperatura
                      ) > objetoPlantacao.configuracao.temperatura_max) ? (
                      <Tip
                        label="Temperatura fora dos parâmetros"
                        fontSize="md"
                      >
                        <WarningTwoIcon w={5} h={5} color="red.500" />
                      </Tip>
                    ) : null}
                  </Text>
                </Center>
                <Center>
                  <Flex flexDirection="row">
                    <Text fontSize="sm">
                      {objetoDados &&
                      objetoDados.dados &&
                      objetoDados.dados.length > 0
                        ? Number(
                            objetoDados.dados[objetoDados.dados.length - 1]
                              .temperatura
                          ).toFixed(2)
                        : '-'}
                    </Text>
                  </Flex>
                </Center>
              </CardHeader>
            </Card>
          </GridItem>

          <Card maxH="15vh">
            <CardHeader>
              <Center>
                <Heading size="sm">Umidade do solo (%)</Heading>
                <Text marginLeft={1}>
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoPlantacao.configuracao &&
                  objetoDados.dados.length > 0 &&
                  (Number(
                    objetoDados.dados[objetoDados.dados.length - 1].umidade_solo
                  ) < objetoPlantacao.configuracao.umidade_solo_min ||
                    Number(
                      objetoDados.dados[objetoDados.dados.length - 1]
                        .umidade_solo
                    ) > objetoPlantacao.configuracao.umidade_solo_max) ? (
                    <Tip
                      label="Umidade do solo fora dos parâmetros"
                      fontSize="md"
                    >
                      <WarningTwoIcon w={5} h={5} color="red.500" />
                    </Tip>
                  ) : null}
                </Text>
              </Center>
              <Center>
                <Text fontSize="sm">
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoDados.dados.length > 0
                    ? Number(
                        objetoDados.dados[objetoDados.dados.length - 1]
                          .umidade_solo
                      ).toFixed(2)
                    : '-'}
                </Text>
              </Center>
            </CardHeader>
          </Card>
          <Card maxH="15vh">
            <CardHeader>
              <Center>
                <Heading size="sm">Umidade do ar (%)</Heading>
                <Text marginLeft={1}>
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoPlantacao.configuracao &&
                  objetoDados.dados.length > 0 &&
                  (Number(
                    objetoDados.dados[objetoDados.dados.length - 1].umidade_ar
                  ) < objetoPlantacao.configuracao.umidade_ar_min ||
                    Number(
                      objetoDados.dados[objetoDados.dados.length - 1].umidade_ar
                    ) > objetoPlantacao.configuracao.umidade_ar_max) ? (
                    <Tip
                      label="Umidade do ar fora dos parâmetros"
                      fontSize="md"
                    >
                      <WarningTwoIcon w={5} h={5} color="red.500" />
                    </Tip>
                  ) : null}
                </Text>
              </Center>
              <Center>
                <Text fontSize="sm">
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoDados.dados.length > 0
                    ? Number(
                        objetoDados.dados[objetoDados.dados.length - 1]
                          .umidade_ar
                      ).toFixed(2)
                    : '-'}
                </Text>
              </Center>
            </CardHeader>
          </Card>
          <Card maxH="15vh">
            <CardHeader>
              <Center>
                <Heading size="sm">Velocidade do vento (km/h)</Heading>
                <Text marginLeft={1}>
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoPlantacao.configuracao &&
                  objetoDados.dados.length > 0 &&
                  (Number(
                    objetoDados.dados[objetoDados.dados.length - 1]
                      .velocidade_ar
                  ) < objetoPlantacao.configuracao.velocidade_ar_min ||
                    Number(
                      objetoDados.dados[objetoDados.dados.length - 1]
                        .velocidade_ar
                    ) > objetoPlantacao.configuracao.velocidade_ar_max) ? (
                    <Tip
                      label="Velocidade do vento fora dos parâmetros"
                      fontSize="md"
                    >
                      <WarningTwoIcon w={5} h={5} color="red.500" />
                    </Tip>
                  ) : null}
                </Text>
              </Center>
              <Center>
                <Text fontSize="sm">
                  {objetoDados &&
                  objetoDados.dados &&
                  objetoDados.dados.length > 0
                    ? Number(
                        objetoDados.dados[objetoDados.dados.length - 1]
                          .velocidade_ar
                      ).toFixed(2)
                    : '-'}
                </Text>
              </Center>
            </CardHeader>
          </Card>
        </Grid>
        <Card>
          <CardHeader>
            <Center>
              <Heading size="sm">Histórico</Heading>
            </Center>
            <Grid templateColumns="repeat(2, 1fr)" gap={2}>
              <Card size="lg">
                <CardHeader>
                  <Center>
                    <Heading size="sm">Temperatura</Heading>
                  </Center>
                  <Center>
                    <Line
                      data={createChartData(
                        'Temperatura',
                        listaTemperatura,
                        '#B6452F',
                        '#B6452F'
                      )}
                      options={options}
                    />
                  </Center>
                </CardHeader>
              </Card>
              <Card size="lg">
                <CardHeader>
                  <Center>
                    <Heading size="sm">Umidade do solo</Heading>
                  </Center>
                  <Center>
                    <Line
                      data={createChartData(
                        'Umidade do solo',
                        listaUmidadeSolo,
                        '#FAB180',
                        '#CAAA82'
                      )}
                      options={options}
                    />
                  </Center>
                </CardHeader>
              </Card>
              <Card size="lg">
                <CardHeader>
                  <Center>
                    <Heading size="sm">Umidade do ar</Heading>
                  </Center>
                  <Center>
                    <Line
                      data={createChartData(
                        'Umidade do ar',
                        listaUmidadeAr,
                        '#2077F4',
                        '#3077F4'
                      )}
                      options={options}
                    />
                  </Center>
                </CardHeader>
              </Card>
              <Card size="lg">
                <CardHeader>
                  <Center>
                    <Heading size="sm">Velocidade do vento</Heading>
                  </Center>
                  <Center>
                    <Line
                      data={createChartData(
                        'Velocidade do vento',
                        listaVelocidadeAr,
                        '#33AF23',
                        '#44FF2F'
                      )}
                      options={options}
                    />
                  </Center>
                </CardHeader>
              </Card>
            </Grid>
          </CardHeader>
        </Card>
      </Box>
    </Stack>
  );
}
