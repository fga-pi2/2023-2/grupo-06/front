import { useToast } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { useMutation } from '@tanstack/react-query';
import { signUp } from '../../features/users/api';
import { UserForm } from '../../features/users/CreateOrUpdateUser';

export function SignUpPage() {
  const toast = useToast({ position: 'top' });
  const navigateTo = useNavigate();
  const mutation = useMutation({
    mutationFn: signUp,
    onSuccess: () => {
      toast({
        title: 'Conta criada com sucesso',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });
      setTimeout(() => {
        navigateTo('/login');
      }, 2000);
    },
    onError: (err) => {
      toast({
        title: 'Erro ao criar conta.',
        description: err.message,
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });

  return <UserForm submit={mutation.mutate} />;
}
