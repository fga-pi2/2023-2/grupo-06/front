import { render, screen } from '@testing-library/react';

import { SignUpPage } from '..';
import { Wrapper } from '../../../utils/testWrapper';

describe('Tela de cadastro', () => {
  it('deve renderizar a tela de cadastro', () => {
    render(
      <Wrapper>
        <SignUpPage />
      </Wrapper>
    );
    const text = screen.getByText('Cadastro');
    expect(text).toBeInTheDocument();
  });
});
