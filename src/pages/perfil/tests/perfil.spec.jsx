import { render, screen } from '@testing-library/react';

import { Profile } from '..';
import { Wrapper } from '../../../utils/testWrapper';

describe('Tela de perfil', () => {
  it('deve renderizar a tela de perfil', () => {
    render(
      <Wrapper>
        <Profile />
      </Wrapper>
    );
    const text = screen.getByText('Perfil');
    expect(text).toBeInTheDocument();
  });
});
