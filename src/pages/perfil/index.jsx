import { useQuery, useMutation } from '@tanstack/react-query';
import { useToast } from '@chakra-ui/react';
import { useEffect } from 'react';
import { UserForm } from '../../features/users/CreateOrUpdateUser';
import { getByEmail, getById, putUserData } from '../../features/users/api';

export function Profile() {
  const toast = useToast();
  const savedData = JSON.parse(localStorage.getItem('userValue'));
  const userId = savedData ? savedData.id : '';

  const query = useQuery({
    queryKey: ['GET_USER', userId],
    queryFn: async () => getById(userId),
    retry: false,
  });

  const user = query?.data;

  useEffect(() => {
    if (query.error) {
      toast({
        position: 'top',
        status: 'error',
        isClosable: true,
        title: 'Erro ao obter dados do usuário',
        description: query.error.message,
      });
    }
  }, [query.error, toast]);

  const mutation = useMutation({
    mutationFn: putUserData,
    onSuccess: () => {
      toast({
        title: 'Conta editada com sucesso',
        description: '',
        status: 'success',
        duration: 6000,
        isClosable: true,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    },
    onError: (err) => {
      toast({
        title: 'Erro ao editar conta.',
        description: err.message,
        status: 'error',
        duration: 6000,
        isClosable: true,
      });
    },
  });

  return (
    <UserForm
      userData={{ name: user?.nome, email: user?.email }}
      submit={mutation.mutate}
    />
  );
}
