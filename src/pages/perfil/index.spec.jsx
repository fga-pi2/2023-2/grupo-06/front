import { render, screen } from '@testing-library/react';
import { Profile } from './index';
import { Wrapper } from '../../utils/testWrapper';

describe('Profile', () => {
  it('renders the title', () => {
    render(
      <Wrapper>
        <Profile />
      </Wrapper>
    );
    const title = screen.getByText('Nome');
    expect(title).toBeInTheDocument();
  });
});
