import { signIn } from '../../features/users/api';
import { SignIn } from '../../features/users/userSignIn';

export function SignInPage() {
  return <SignIn submit={signIn} />;
}
