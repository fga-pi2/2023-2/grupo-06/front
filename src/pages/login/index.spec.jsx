import { render, screen } from '@testing-library/react';

import { SignInPage } from './index';
import { Wrapper } from '../../utils/testWrapper';

describe('Tela de login', () => {
  it('renders the page', () => {
    render(
      <Wrapper>
        <SignInPage />
      </Wrapper>
    );
    const title = screen.getByText('E-mail');
    expect(title).toBeInTheDocument();
  });
});
