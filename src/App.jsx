import { BrowserRouter } from 'react-router-dom';
import { ChakraProvider } from '@chakra-ui/react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { Router } from './config/routes/Routes';
import { theme } from './styles/theme';

const queryClient = new QueryClient();

export function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <ChakraProvider resetCSS theme={theme}>
          <Router />
        </ChakraProvider>
      </BrowserRouter>
    </QueryClientProvider>
  );
}
