import { defineConfig } from 'vitest/config';
import react from '@vitejs/plugin-react-swc';

export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    coverage: { reporter: ['lcov', 'html', 'cobertura'] },
    environment: 'jsdom',
    setupFiles: './src/config/tests/setup-tests.ts',
    outputFile: 'coverage/coverage.xml',
  },
});
