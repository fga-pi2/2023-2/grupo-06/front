import { defineConfig, loadEnv } from 'vite';
import react from '@vitejs/plugin-react-swc';
import viteCompression from 'vite-plugin-compression';
import zlib from 'zlib';

export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '');
  return {
    define: {
      'process.env.REACT_APP_API_URL': JSON.stringify(env.REACT_APP_API_URL),
    },
    plugins: [
      react(),
      viteCompression({
        algorithm: 'brotliCompress',
        threshold: 1024,
        compressionOptions: {
          params: { [zlib.constants.BROTLI_PARAM_QUALITY]: 11 },
        },
      }),
      viteCompression({
        algorithm: 'gzip',
        threshold: 1024,
      }),
    ],
    server: {
      port: 3000,
      open: true,
      watch: {
        usePolling: true,
      },
    },
    optimizeDeps: {
      esbuildOptions: {
        define: {
          global: 'globalThis',
        },
      },
    },
  };
});
